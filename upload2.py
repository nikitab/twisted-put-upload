from twisted.internet import reactor, protocol
from twisted.protocols import basic   
import re          
import string 
import time
                            

class Uploader(basic.LineReceiver): 
    delimiter = '\r\n'                   
    
    def __init__(self):
        self.filename = None
    
    def connectionMade(self):
        print "Got connection from %s!" % self.transport.getPeer().host
        
    def lineReceived(self, line):
        print "Got line: [%s]" % line
        if not self.filename:
            m = re.match(r"PUT (\S+) HTTP", line, re.IGNORECASE)
            if not m:                          
                print "Invalid request"
                self.transport.loseConnection()                               
                return
            self.filename = self.transport.getPeer().host + '/' + re.sub('[^A-Za-z0-9-_.]+', '', m.group(1)) + '--' + str(time.time())
            print "Filename is: ", self.filename         
	    try:
		    self.file = open(self.filename, 'w')
	    except:
                print "Cannot create file"
                self.transport.loseConnection()
            return
        m = re.match(r"Content-Length: (\d+)", line, re.IGNORECASE)
        if m:
            self.content_length = int(m.group(1)) 
            print "Content length is:", self.content_length
        if line == "":                                   
            print "Continue"
            self.transport.write("HTTP/1.1 100 CONTINUE\r\n\r\n")
            self.setRawMode()
            
    def rawDataReceived(self, data):
        print "Got data [%d]" % (len(data)) 
        self.file.write(data)               
        self.content_length -= len(data)
        if self.content_length == 0:
            self.file.close()
            self.transport.write("HTTP/1.1 200 OK\r\nConnection: close\r\nContent-Length: 0\r\n\r\n")
            print "Finished"
            
class UploadFactory(protocol.ServerFactory):
    protocol = Uploader
                                                              
if __name__ == '__main__':
    import sys
    reactor.listenTCP(int(sys.argv[1]), UploadFactory())
    reactor.run()
       
